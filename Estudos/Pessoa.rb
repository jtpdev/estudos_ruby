class Pessoa
  def initialize()
    set_nome("João")
  end

#    def initialize(nome)
#      set_nome(nome)
#    end

  def set_nome(nome)
    @nome = nome;
  end

  def get_nome
    @nome
  end

  def nome=(novo_nome)
    @nome = novo_nome
  end

  def oi
    puts "oi"
    yield nome
  end

#  Aqui dentro não funcionou  
#  oi do |nome|
#    puts "meu nome é #{nome}"
#  end

  protected

  def fala()
    puts "Sei falar"
  end
  
end