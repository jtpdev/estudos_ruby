=begin
  Conjunto de métodos ou classes que podem ser utilizados em outro lugar
=end

module Modulo
  def metodo
    puts "estou em um módulo!"
  end
  module OutroModulo
    def metodo_incluso
      puts "sou um método de #{self.class}"
    end
    class ClasseInclusa
      def metodo_incluso_em_classe_inclusa
        puts "sou uma classe de #{self.class}"
      end
    end
  end
end