=begin
  continuar de: 3.21 - Manipulando erros e exceptions
=end
require_relative "Pessoa"
#require "Pessoa"

class Hello_World
  #  puts(3+4-2 <= 3*2/4)
  #  a = nil
  #  if(!a)
  #    puts("value of #{nil}")
  #  end
  def existe(em, texto)
    if(/.*#{texto}/ =~ em)
      #  if(/.*lalalala/ =~ "asdasdasdas")
      puts("existe")
    elsif
    puts("não existe")
    end
  end

  def testa (ze)
    case ze
    when 1..3
      "1..3"
    when 1...3
      "1...3"
    when 4
      "#{ze}"
    end
  end

  def testa2 (ze)
    case ze
    when 1..3
      puts "1..3"
    when 1...3
      puts "1...3"
    when 4
      puts "#{ze}"
    end
  end

  case 3 # Se 3 não entra em nenhum nesse caso
  #      when 1..3
  #        puts "1..3"
  when 1...3
    puts "1...3"
  when 4
    puts "4"
  end

  #  puts testa(1) Porque aqui não funciona?

  # requisição http
  def req (site)
    require 'net\http'
    Net::HTTP.start( site, 80 ) do |http|
      print( http.get( '\\' ).body )
    end
  end

  # escrever e ler arquivo
  def get_file(name)
    print "Escreva um texto: "
    texto = gets
    File::open( "#{name}.txt", "w" ) do |f|
      f << texto
    end
    Dir["#{name}.txt"].each do |file_name|
      idea = File.read( file_name )
      puts idea
    end
  end

  # Se valor for nulo imprimirá 10
  def pipe(valor)
    n = valor ||= 10
    puts n
  end

  def falar()
    pessoa = Object.new()
    def pessoa.fala()
      puts "Sei falar"
    end
    pessoa
  end

  def add(value, to=1)
    value + to
  end

  def transfere(argumentos)
  destino = argumentos[:destino]
  valor = argumentos[:valor]
  # ...
    puts argumentos
  end

  
end

o = Hello_World.new()
#o.transfere :destino => outra_conta = Object.new, :valor => 10.25 # Hash em Ruby
#puts o.testa(4)
#puts o.testa2(1)
#o.req('www.google.com')
#o.get_file("Teste")
#o.pipe(nil)
#o.existe("lala","laa")
#o.falar().fala() # nossa! como isso é fácil
#puts o.add(30) # com segundo parâmetro, soma o valor setado, sem soma 1
#puts o.send(:falar).fala() # util para reflexão
#puts :teste # Symbol não String, são imutáveis e compartilhados todos os lugares da aplicação com :teste referenciam o mesmo objeto
#puts 1.class() # Classe Fixnum
#puts 1.object_id() # Identificador único do objeto

class Pessoa
  def outra()
    puts "Sei falar"
  end
  
  def nome
    @nome
  end
  
#  fala() # entender como funciona
end

p = Pessoa.new #"Zénildo" # initialize funciona como construtor da classe, caso crie um initialize com parâmetros terei de passar os parametros, mesmo que defina um initialize sem parâmetros
#p.fala() # Acessado da classe pessoa (PEssoa.rb)
#p.set_nome("ze") # puts p.set_nome("ze") também funciona
#puts p.get_nome
#p.nome = "José" # puts p.nome=("José") também funciona
#puts p.nome

#p.oi do |nome| # método funcional
#  puts "meu nome é #{nome}"
#end

#lista = ["4", "um", "cinco", "bla"] # for funcional
#lista.each do |item|
#puts item
#end

teste = [
  {:ruby => 'rr-71', :java => 'fj-11'},{:ruby => 'rr-75', :java => 'fj-21'}
]
teste.sort_by { |curso| curso[:ruby] }.each do |curso|
puts "Curso de RoR na Caelum: #{ curso[:ruby] }"
end
puts "==============="
teste.sort_by { |curso| curso[:ruby] }.each do |curso|
next if curso[:ruby] == 'rr-71'
puts "Curso de RoR na Caelum: #{ curso[:ruby] }"
end

#instance = Class.new
#another_instance = instance.new
#puts another_instance.class

#class << SingletonClass # para transformar a classe em singleton, não podendo ser instanciada
#  def s.teste # para ser usado apenas na classe SingletonClass
#    puts "teste"
#  end
#end
#
#s = SingletonClass.new
#s.teste

#class Aluno
#  attr_accessor :escreve # metaprogramação: define escreve e escreve= (getter e setter)
#  private # visibilidade de métodos
#  def testa
#  end
#  protected
#  def testa_outro
#  end
#end


#juca = Aluno.new
#puts juca.respond_to? :escreve
#puts juca.escreve = "sei escrever"
#juca.testa_outro
#
#  ou
#
#class Aluno
#  # nao sabe nada
#end
#
#class Professor
#  def ensina(aluno)
#    def aluno.escreve
#      "sei escrever!"
#    end
#  end
#end
#
#juca = Aluno.new
#puts juca.respond_to? :escreve
#professor = Professor.new
#professor.ensina juca
#puts juca.escreve

